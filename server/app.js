const express = require('express');
const router = require('./review/review.routes');

const app = express();
app.use(express.json());

// Weiterleitung von Anfragen an json-server
app.use('/reviews', router);
    
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`);
});
