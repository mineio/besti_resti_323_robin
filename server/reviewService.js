const fs = require('fs');
const path = require('path');
const Review = require('./review/review.model.js');

const dbPath = path.join(__dirname, '/data', 'db.json');

const readDB = () => {
  const data = fs.readFileSync(dbPath, 'utf8');
  return JSON.parse(data);
};

const writeDB = (data) => {
  fs.writeFileSync(dbPath, JSON.stringify(data, null, 2), 'utf8');
};

const getNextId = (reviews) => {
  return reviews.length > 0 ? Math.max(...reviews.map(r => r.id)) + 1 : 1;
};

const reviewService = {
  getAllReviews: () => {
    const db = readDB();
    return Promise.resolve(db.reviews);
  },

  getReviewById: (id) => {
    const db = readDB();
    const review = db.reviews.find(r => r.id === parseInt(id));
    return review ? Promise.resolve(review) : Promise.reject('Bewertung nicht gefunden');
  },

  addReview: (reviewData) => {
    const db = readDB();
    const newReview = new Review({
      id: getNextId(db.reviews),
      ...reviewData
    });
    db.reviews.push(newReview);
    writeDB(db);
    return Promise.resolve(newReview);
  },
  

  updateReview: (id, updateData) => {
    const db = readDB();
    const index = db.reviews.findIndex(r => r.id === parseInt(id));
    if (index !== -1) {
      const updatedReview = { ...db.reviews[index], ...updateData };
      db.reviews[index] = updatedReview;
      writeDB(db);
      return Promise.resolve(updatedReview);
    }
    return Promise.reject('Bewertung nicht gefunden');
  },

  deleteReview: (id) => {
    const db = readDB();
    const newReviews = db.reviews.filter(r => r.id !== parseInt(id));
    if (db.reviews.length !== newReviews.length) {
      db.reviews = newReviews;
      writeDB(db);
      return Promise.resolve('Bewertung gelöscht');
    }
    return Promise.reject('Bewertung nicht gefunden');
  }
};

module.exports = reviewService;
