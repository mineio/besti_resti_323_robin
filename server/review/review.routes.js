const { Router } = require('express');

const {getAllReviews, getReviewById, addReview, updateReview, deleteReview } = require('./review.controller');
const router = Router();

// Get all reviews
router.get('/', getAllReviews);

// Get a single review by ID
router.get('/:id', getReviewById);

// Add a new review
router.post('/', addReview);

// Update a review by ID
router.put('/:id', updateReview);

// Delete a review by ID
router.delete('/:id', deleteReview);

module.exports = router;
