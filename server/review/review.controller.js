const reviewService = require('../reviewService');

const reviewController = {
  getAllReviews: (req, res) => {
    reviewService.getAllReviews()
      .then(reviews => res.json(reviews))
      .catch(err => res.status(500).send(err));
  },

  getReviewById: (req, res) => {
    reviewService.getReviewById(req.params.id)
      .then(review => res.json(review))
      .catch(err => res.status(404).send(err));
  },

  addReview: (req, res) => {
    reviewService.addReview(req.body)
      .then(review => res.status(201).json(review))
      .catch(err => res.status(400).send(err));
  },

  updateReview: (req, res) => {
    reviewService.updateReview(req.params.id, req.body)
      .then(review => res.json(review))
      .catch(err => res.status(400).send(err));
  },

  deleteReview: (req, res) => {
    reviewService.deleteReview(req.params.id)
      .then(msg => res.send(msg))
      .catch(err => res.status(404).send(err));
  }
};

module.exports = reviewController;
