class Review {
    constructor({ id, restaurantName, address, cheapestMenu, mostExpensiveWine, rating, reviewDate, comment, googleMapsLink }) {
      this.id = id;
      this.restaurantName = restaurantName;
      this.address = address;
      this.cheapestMenu = cheapestMenu;
      this.mostExpensiveWine = mostExpensiveWine;
      this.rating = rating;
      this.reviewDate = reviewDate;
      this.comment = comment;
      this.googleMapsLink = googleMapsLink;
    }
  }
  
  module.exports = Review;
  