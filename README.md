# Installationsanleitung
1. Mit der console in das root Verzeichnis(Besi_Resti_323_Robin) navigieren.
2. npm run install-all ausführen
3. Applikation mit npm start starten
   
Zu beachten:
Die Ports 3000 und 5000 werden benötigt und müssen daher frei sein.


# getesteten Komponenten inklusive Begründung
Review.js: Wird getestet, da es die Basis für die ganze Anwendung bildet, weil es dafür sorgt, dass die Reviews korrekt angezeigt werden.
ReviewForm.js: Wird getestet, da es die Funktionalität für das hinzufügen einer Bewertung enthält, ohne welche die gesamte Anwedung ziemlich nutzlos ist.
Search.js: Wird getesetet, da es die Funktionalität für die Suchfunktionen enthält, welche den Grund bildet die Anwedung überhaupt zu benutzen (Restaurants mit bestimmten Anforderung finden).

# nicht getesteten Komponenten inklusive Begründung
App.js: App.js wird nicht mit Jest getestet, da es nur die CRUD Funktionen enthält, welche 1. relativ simpel sind und 2. von einer Person sehr schnell getestet werden können, indem sie auf der Webseite eine Bewertung hinzufügt, diese bearbeitet und anschliessend wieder löscht, was weniger als eine Minute dauert.