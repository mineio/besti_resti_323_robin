import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import SearchComponent from '../components/Search';

describe('SearchComponent Tests', () => {
  const mockReviews = [
    { id: 1, restaurantName: 'Restaurant A', rating: 5, mostExpensiveWine: { price: 100 }, cheapestMenu: { price: 20 } },
    { id: 2, restaurantName: 'Restaurant B', rating: 4, mostExpensiveWine: { price: 80 }, cheapestMenu: { price: 15 } },
    { id: 3, restaurantName: 'Restaurant C', rating: 3, mostExpensiveWine: { price: 60 }, cheapestMenu: { price: 10 } },
    { id: 4, restaurantName: 'Restaurant D', rating: 2, mostExpensiveWine: { price: 50 }, cheapestMenu: { price: 5 } },
    { id: 5, restaurantName: 'Restaurant E', rating: 1, mostExpensiveWine: { price: 30 }, cheapestMenu: { price: 25 } },
    { id: 6, restaurantName: 'Restaurant F', rating: 5, mostExpensiveWine: { price: 120 }, cheapestMenu: { price: 30 } }
  ];

  it('should render the search component correctly', () => {
    render(<SearchComponent reviews={mockReviews} onSearchResults={() => {}} />);
    expect(screen.getByText('Select Search Type')).toBeInTheDocument();
  });

  it('should display top five restaurants by stars when selected', () => {
    const mockOnSearchResults = jest.fn();
    render(<SearchComponent reviews={mockReviews} onSearchResults={mockOnSearchResults} />);
    fireEvent.change(screen.getByRole('combobox'), { target: { value: 'stars' } });
    expect(mockOnSearchResults).toHaveBeenCalledWith(expect.arrayContaining([
      expect.objectContaining({ restaurantName: 'Restaurant A' }),
      expect.objectContaining({ restaurantName: 'Restaurant F' })
    ]));
    expect(mockOnSearchResults).toHaveBeenCalledTimes(1);
  });

  it('should search and display restaurants based on wine price', () => {
    const mockOnSearchResults = jest.fn();
    render(<SearchComponent reviews={mockReviews} onSearchResults={mockOnSearchResults} />);
    fireEvent.change(screen.getByRole('combobox'), { target: { value: 'winePrice' } });
    fireEvent.change(screen.getByPlaceholderText('Enter value'), { target: { value: '50' } });
    fireEvent.click(screen.getByText('Search'));
    expect(mockOnSearchResults).toHaveBeenCalledWith(expect.arrayContaining([
      expect.objectContaining({ restaurantName: 'Restaurant A' }),
      expect.objectContaining({ restaurantName: 'Restaurant B' })
    ]));
    expect(mockOnSearchResults).toHaveBeenCalledTimes(1);
  });

  it('should search and display restaurants based on menu price', () => {
    const mockOnSearchResults = jest.fn();
    render(<SearchComponent reviews={mockReviews} onSearchResults={mockOnSearchResults} />);
    fireEvent.change(screen.getByRole('combobox'), { target: { value: 'menuPrice' } });
    fireEvent.change(screen.getByPlaceholderText('Enter value'), { target: { value: '20' } });
    fireEvent.click(screen.getByText('Search'));
    expect(mockOnSearchResults).toHaveBeenCalledWith(expect.arrayContaining([
      expect.objectContaining({ restaurantName: 'Restaurant C' }),
      expect.objectContaining({ restaurantName: 'Restaurant D' })
    ]));
    expect(mockOnSearchResults).toHaveBeenCalledTimes(1);
  });
});
