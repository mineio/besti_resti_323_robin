import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Review from "../components/Review";

describe("Review Component Tests", () => {
  const mockReview = {
    id: 1,
    restaurantName: "Test Restaurant",
    address: "123 Test Street",
    cheapestMenu: { name: "Test Menu", price: 10 },
    mostExpensiveWine: { name: "Test Wine", price: 50 },
    rating: 4,
    reviewDate: "2023-01-01",
    comment: "Great place!",
    googleMapsLink: "http://maps.google.com/test",
  };

  const mockDelete = jest.fn();
  const mockEdit = jest.fn();

  it("renders correctly with given props", () => {
    render(<Review {...mockReview} onDelete={mockDelete} onEdit={mockEdit} />);
    expect(screen.getByText("Test Restaurant")).toBeInTheDocument();
    expect(screen.getByText("123 Test Street")).toBeInTheDocument();
    expect(screen.getByText("Test Menu - Price: 10 €")).toBeInTheDocument();
    expect(screen.getByText("Test Wine - Price: 50 €")).toBeInTheDocument();
    expect(screen.getByText("4 Stars")).toBeInTheDocument();
    expect(screen.getByText("2023-01-01")).toBeInTheDocument();
    expect(screen.getByText("Great place!")).toBeInTheDocument();
    expect(screen.getByText("Show on Google Maps")).toHaveAttribute("href", "http://maps.google.com/test");
  });

  it("calls onDelete when delete button is clicked", () => {
    render(<Review {...mockReview} onDelete={mockDelete} onEdit={mockEdit} />);
    fireEvent.click(screen.getByText("Delete"));
    expect(mockDelete).toHaveBeenCalledWith(mockReview.id);
  });

  it("calls onEdit when edit button is clicked", () => {
    render(<Review {...mockReview} onDelete={mockDelete} onEdit={mockEdit} />);
    fireEvent.click(screen.getByText("Edit"));
    expect(mockEdit).toHaveBeenCalledWith(mockReview.id);
  });

});

