import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ReviewForm from "../components/ReviewForm";


const FIXED_DATE = '2023-12-06'; 

// Fixiertes Datum setzen
const originalDate = global.Date;
beforeAll(() => {
  global.Date = class extends Date {
    constructor() {
      super();
      return new originalDate(FIXED_DATE);
    }
  };
});

// Datum wiederherstellen
afterAll(() => {
  global.Date = originalDate;
});

describe("ReviewForm Component Tests", () => {
  const mockOnSubmit = jest.fn();

  beforeEach(() => {
    render(<ReviewForm onSubmit={mockOnSubmit} />);
  });

  it("renders form with all inputs and a submit button", () => {
    expect(screen.getByPlaceholderText("Restaurant name")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Address")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Cheapest menu name")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Cheapest menu price")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Most expensive wine name")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Most expensive wine price")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Rating (1-5)")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Comment")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Google Maps link")).toBeInTheDocument();
    expect(screen.getByRole("button", { name: "Add Review" })).toBeInTheDocument();
  });

  it("allows input to be entered in form fields", () => {
    userEvent.type(screen.getByPlaceholderText("Restaurant name"), "Test Restaurant");
    expect(screen.getByPlaceholderText("Restaurant name")).toHaveValue("Test Restaurant");

    userEvent.type(screen.getByPlaceholderText("Address"), "123 Test Street");
    expect(screen.getByPlaceholderText("Address")).toHaveValue("123 Test Street");

    userEvent.type(screen.getByPlaceholderText("Cheapest menu name"), "Test Menu");
    expect(screen.getByPlaceholderText("Cheapest menu name")).toHaveValue("Test Menu");

    userEvent.type(screen.getByPlaceholderText("Cheapest menu price"), "10");
    expect(screen.getByPlaceholderText("Cheapest menu price")).toHaveValue(10);

    userEvent.type(screen.getByPlaceholderText("Most expensive wine name"), "Test Wine");
    expect(screen.getByPlaceholderText("Most expensive wine name")).toHaveValue("Test Wine");

    userEvent.type(screen.getByPlaceholderText("Most expensive wine price"), "50");
    expect(screen.getByPlaceholderText("Most expensive wine price")).toHaveValue(50);

    userEvent.type(screen.getByPlaceholderText("Rating (1-5)"), "4");
    expect(screen.getByPlaceholderText("Rating (1-5)")).toHaveValue(4);

    userEvent.type(screen.getByPlaceholderText("Comment"), "Great place!");
    expect(screen.getByPlaceholderText("Comment")).toHaveValue("Great place!");

    userEvent.type(screen.getByPlaceholderText("Google Maps link"), "http://maps.google.com/test");
    expect(screen.getByPlaceholderText("Google Maps link")).toHaveValue("http://maps.google.com/test");
  });

  it("calls onSubmit with the form data when form is submitted", () => {
    userEvent.type(screen.getByPlaceholderText("Restaurant name"), "Test Restaurant");
    userEvent.type(screen.getByPlaceholderText("Address"), "123 Test Street");
    userEvent.type(screen.getByPlaceholderText("Cheapest menu name"), "Test Menu");
    userEvent.type(screen.getByPlaceholderText("Cheapest menu price"), "10");
    userEvent.type(screen.getByPlaceholderText("Most expensive wine name"), "Test Wine");
    userEvent.type(screen.getByPlaceholderText("Most expensive wine price"), "50");
    userEvent.type(screen.getByPlaceholderText("Rating (1-5)"), "4");
    userEvent.type(screen.getByPlaceholderText("Comment"), "Great place!");
    userEvent.type(screen.getByPlaceholderText("Google Maps link"), "http://maps.google.com/test");

    fireEvent.submit(screen.getByRole("button", { name: "Add Review" }));

    expect(mockOnSubmit).toHaveBeenCalledWith({
      restaurantName: "Test Restaurant",
      address: "123 Test Street",
      cheapestMenu: { name: "Test Menu", price: 10 },
      mostExpensiveWine: { name: "Test Wine", price: 50 },
      rating: "4",
      reviewDate: FIXED_DATE,
      comment: "Great place!",
      googleMapsLink: "http://maps.google.com/test",
    });
  });
});
