import React, { useState, useEffect } from "react";
import Review from "./components/Review";
import ReviewForm from "./components/ReviewForm";
import SearchComponent from "./components/Search";
import ReviewService from "./reviewService";

function App() {
  const [reviews, setReviews] = useState([]);
  const [filteredReviews, setFilteredReviews] = useState([]);
  const [showAddForm, setShowAddForm] = useState(false);
  const [editingReview, setEditingReview] = useState(null);

  useEffect(() => {
    fetchReviews();
  }, []);

  const fetchReviews = () => {
    ReviewService.getReviews()
      .then(response => {
        setReviews(response.data);
        setFilteredReviews(response.data.sort((a, b) => new Date(b.reviewDate) - new Date(a.reviewDate)).slice(0, 3));
      })
      .catch(error => console.error("Error fetching data:", error));
  };

  const addReview = (newReview) => {
    const existing = reviews.find(
      review => review.restaurantName === newReview.restaurantName && review.address === newReview.address
    );
    if (existing) {
      alert('A review for this restaurant already exists.');
      return;
    }
    ReviewService.addReview(newReview)
      .then(() => {
        fetchReviews();
        setShowAddForm(false);
      })
      .catch(error => console.error("Error posting data:", error));
  };

  const updateReview = (updatedReview) => {
    ReviewService.updateReview(updatedReview.id, updatedReview)
      .then(() => {
        fetchReviews();
        setEditingReview(null);
      })
      .catch(error => console.error("Error updating data:", error));
  };

  const deleteReview = (id) => {
    ReviewService.deleteReview(id)
      .then(() => {
        fetchReviews();
      })
      .catch(error => console.error("Error deleting data:", error));
  };

  const startEdit = (review) => {
    setEditingReview(review);
    setShowAddForm(false);
  };

  const updateFilteredReviews = (results) => {
    setFilteredReviews(results);
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Restaurant Reviews</h1>
        <button onClick={() => { setShowAddForm(!showAddForm); setEditingReview(null); }}>
          {showAddForm ? "Hide Add Review Form" : "Add Review"}
        </button>
        <SearchComponent reviews={reviews} onSearchResults={updateFilteredReviews} />
      </header>
      {showAddForm && <ReviewForm onSubmit={addReview} />}
      {editingReview && <ReviewForm reviewToEdit={editingReview} onSubmit={updateReview} />}
      <div className="reviews">
        {filteredReviews.map((review) => (
          <Review key={review.id} {...review} onDelete={deleteReview} onEdit={() => startEdit(review)} />
        ))}
      </div>
    </div>
  );
}

export default App;
