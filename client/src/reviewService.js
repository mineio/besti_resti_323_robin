import axios from 'axios';

const baseURL = 'http://localhost:5000/reviews';

const ReviewService = {
  getReviews: async () => {
    return axios.get(baseURL);
  },
  addReview: async (review) => {
    return axios.post(baseURL, review);
  },
  updateReview: async (id, review) => {
    return axios.put(`${baseURL}/${id}`, review);
  },
  deleteReview: async (id) => {
    return axios.delete(`${baseURL}/${id}`);
  }
};

export default ReviewService;
