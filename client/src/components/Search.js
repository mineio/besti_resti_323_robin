import React, { useState, useEffect } from "react";

const SearchComponent = ({ reviews, onSearchResults }) => {
  const [searchType, setSearchType] = useState('');
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    if (searchType === 'stars') {
      const topFiveRated = reviews
        .sort((a, b) => b.rating - a.rating)
        .slice(0, 5);
      onSearchResults(topFiveRated);
    }
  }, [searchType, reviews, onSearchResults]);

  const handleSearch = () => {
    let results = [];
    if (searchType === 'winePrice') {
      results = reviews
        .filter(review => review.mostExpensiveWine.price >= searchValue)
        .slice(0, 5);
    } else if (searchType === 'menuPrice') {
      results = reviews
        .filter(review => review.cheapestMenu.price <= searchValue)
        .slice(0, 4);
    }
    onSearchResults(results);
  };

  return (
    <div>
      <select onChange={(e) => setSearchType(e.target.value)}>
        <option value="">Select Search Type</option>
        <option value="stars">Stars</option>
        <option value="winePrice">Wine Price</option>
        <option value="menuPrice">Menu Price</option>
      </select>
      {searchType !== 'stars' && (
        <>
          <input type="number" onChange={(e) => setSearchValue(e.target.value)} placeholder="Enter value" />
          <button onClick={handleSearch}>Search</button>
        </>
      )}
    </div>
  );
};

export default SearchComponent;
