import React, { useState, useEffect } from "react";
import "../styles/reviewForm.css";

const ReviewForm = ({ onSubmit, reviewToEdit }) => {
  const [review, setReview] = useState({
    restaurantName: "",
    address: "",
    cheapestMenu: { name: "", price: undefined },
    mostExpensiveWine: { name: "", price: undefined },
    rating: undefined,
    comment: "",
    googleMapsLink: "",
  });

  useEffect(() => {
    if (reviewToEdit) {
      setReview(reviewToEdit);
    } else {
      setReview({
        restaurantName: "",
        address: "",
        cheapestMenu: { name: "", price: undefined },
        mostExpensiveWine: { name: "", price: undefined },
        rating: undefined,
        comment: "",
        googleMapsLink: "",
      });
    }
  }, [reviewToEdit]);

  const [errors, setErrors] = useState({});

  const validateForm = () => {
    let newErrors = {};
    if (!review.restaurantName) newErrors.restaurantName = "Restaurant name is required";
    if (!review.address) newErrors.address = "Address is required";
    if (!review.cheapestMenu.name) newErrors.cheapestMenuName = "Cheapest menu name is required";
    if (review.cheapestMenu.price === undefined) newErrors.cheapestMenuPrice = "Cheapest menu price is required";
    if (!review.mostExpensiveWine.name) newErrors.mostExpensiveWineName = "Most expensive wine name is required";
    if (review.mostExpensiveWine.price === undefined) newErrors.mostExpensiveWinePrice = "Most expensive wine price is required";
    if (review.rating === undefined || review.rating < 1 || review.rating > 5) newErrors.rating = "Rating must be between 1 and 5";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name.includes(".")) {
      const [key, subkey] = name.split(".");
      setReview((prev) => ({
        ...prev,
        [key]: {
          ...prev[key],
          [subkey]: subkey === "price" ? parseFloat(value) : value,
        },
      }));
    } else {
      setReview((prev) => ({ ...prev, [name]: value }));
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!validateForm()) return;

    const currentDate = new Date().toISOString().slice(0, 10);
    onSubmit({ ...review, reviewDate: currentDate });
  };

  return (
    <form className="review-form" onSubmit={handleSubmit}>
      <p className="error">{errors.restaurantName}</p>
      <input
        type="text"
        name="restaurantName"
        value={review.restaurantName}
        onChange={handleChange}
        placeholder="Restaurant name"
      />
      <p className="error">{errors.address}</p>
      <input
        type="text"
        name="address"
        value={review.address}
        onChange={handleChange}
        placeholder="Address"
      />
      <p className="error">{errors.cheapestMenuName}</p>
      <input
        type="text"
        name="cheapestMenu.name"
        value={review.cheapestMenu.name}
        onChange={handleChange}
        placeholder="Cheapest menu name"
      />
      <p className="error">{errors.cheapestMenuPrice}</p>
      <input
        type="number"
        name="cheapestMenu.price"
        value={review.cheapestMenu.price}
        onChange={handleChange}
        placeholder="Cheapest menu price"
      />
      <p className="error">{errors.mostExpensiveWineName}</p>
      <input
        type="text"
        name="mostExpensiveWine.name"
        value={review.mostExpensiveWine.name}
        onChange={handleChange}
        placeholder="Most expensive wine name"
      />
      <p className="error">{errors.mostExpensiveWinePrice}</p>
      <input
        type="number"
        name="mostExpensiveWine.price"
        value={review.mostExpensiveWine.price}
        onChange={handleChange}
        placeholder="Most expensive wine price"
      />
      <p className="error">{errors.rating}</p>
      <input
        type="number"
        name="rating"
        value={review.rating}
        onChange={handleChange}
        placeholder="Rating (1-5)"
        min="1"
        max="5"
      />
      <textarea
        name="comment"
        value={review.comment}
        onChange={handleChange}
        placeholder="Comment"
      ></textarea>
      <input
        type="text"
        name="googleMapsLink"
        value={review.googleMapsLink}
        onChange={handleChange}
        placeholder="Google Maps link"
      />
      <button type="submit">{reviewToEdit ? "Update Review" : "Add Review"}</button>
    </form>
  );
};

export default ReviewForm;
