import React from "react";
import "../styles/review.css";

const Review = ({
  id,
  restaurantName,
  address,
  cheapestMenu,
  mostExpensiveWine,
  rating,
  reviewDate,
  comment,
  googleMapsLink,
  onDelete,
  onEdit,
}) => {
  return (
    <div className="review">
      <h2>{restaurantName}</h2>
      <p>{address}</p>
      <p className="title">Cheapest menu:</p>
      <p>
        {cheapestMenu.name} - Price: {cheapestMenu.price} €
      </p>
      <p className="title">Most expensive wine:</p>
      <p>
        {mostExpensiveWine.name} - Price: {mostExpensiveWine.price} €
      </p>
      <p className="title">Rating:</p>
      <p>{rating} Stars</p>
      <p className="title">Date of rating:</p>
      <p>{reviewDate}</p>
      <p className="title">Comment:</p>
      <p>{comment}</p>
      <a href={googleMapsLink} target="_blank" rel="noopener noreferrer">
        Show on Google Maps
      </a>
      <button onClick={() => onDelete(id)}>Delete</button>{" "}
      <button onClick={() => onEdit(id)}>Edit</button> 
    </div>
  );
};

export default Review;
